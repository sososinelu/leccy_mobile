/*
 * File: app/view/CalculationsZsNorm.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.CalculationsZsNorm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.calculationszsnorm',

    config: {
        layout: {
            align: 'stretchmax',
            type: 'vbox'
        },
        items: [
            {
                xtype: 'container',
                margin: '0 0 12 0',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'container',
                        flex: 0.6
                    },
                    {
                        xtype: 'container',
                        flex: 2,
                        data: {
                            title: 'Mrs',
                            forename: 'Helen',
                            surname: 'Jones',
                            company: 'Helen Electrics',
                            address: '12 High Street, Cathays, Cardiff',
                            postcode: 'SY23 3DS',
                            phone: '01234567890',
                            email: 'helen_j@gmail.com'
                        },
                        margin: '-4 0 0 0',
                        style: 'background-color:#FFF; border-style:solid; border-width: 2px; border-color:#B2CC33; border-radius:6px;',
                        ui: '',
                        width: 200,
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'image',
                                height: 30,
                                margin: '7 0 10 5',
                                width: 30,
                                src: 'resources/zs.png'
                            },
                            {
                                xtype: 'label',
                                flex: 1,
                                centered: false,
                                html: 'Zs Calculator',
                                margin: '9 0 0 0',
                                style: 'text-align:center;color:#000;font-size:24px'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        flex: 0.6
                    }
                ]
            },
            {
                xtype: 'container',
                layout: {
                    align: 'stretchmax',
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'label',
                        html: 'Enter any two values...',
                        margin: '0 0 10 0',
                        style: {
                            'text-align': 'center',
                            'font-size': '15px',
                            'font-style': 'italic'
                        }
                    },
                    {
                        xtype: 'container',
                        margin: '6 0 0 0',
                        layout: {
                            align: 'center',
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'image',
                                height: 40,
                                margin: '0 2 0 15',
                                width: 40,
                                src: 'resources/resistance.png'
                            },
                            {
                                xtype: 'label',
                                html: 'Ze',
                                margin: '0 45 0 5',
                                style: {
                                    'font-style': 'italic',
                                    'font-size': '15px',
                                    color: '#000'
                                }
                            },
                            {
                                xtype: 'numberfield',
                                flex: 1,
                                margin: '0 15 0 0',
                                style: {
                                    'text-align': 'center'
                                },
                                width: '',
                                labelWidth: '0%',
                                name: 'e',
                                placeHolder: 'Enter Ze...'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        items: [
                            {
                                xtype: 'label',
                                docked: 'right',
                                html: 'Ω',
                                margin: '0 20',
                                style: {
                                    color: '#999',
                                    'font-size': '12px',
                                    'font-style': 'italic'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: {
                            align: 'center',
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'image',
                                height: 40,
                                margin: '0 2 0 15',
                                width: 40,
                                src: 'resources/resistance.png'
                            },
                            {
                                xtype: 'label',
                                html: 'R1 +  R2',
                                margin: '0 10 0 5',
                                style: {
                                    'font-style': 'italic',
                                    'font-size': '15px',
                                    color: '#000'
                                }
                            },
                            {
                                xtype: 'numberfield',
                                flex: 1,
                                margin: '0 15 0 0',
                                style: {
                                    'text-align': 'center'
                                },
                                width: '',
                                labelWidth: '0%',
                                name: 'r',
                                placeHolder: 'Enter R1 + R2 ...'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        items: [
                            {
                                xtype: 'label',
                                docked: 'right',
                                html: 'Ω',
                                margin: '0 20',
                                style: {
                                    color: '#999',
                                    'font-size': '12px',
                                    'font-style': 'italic'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: {
                            align: 'center',
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'image',
                                height: 40,
                                margin: '0 2 0 15',
                                width: 40,
                                src: 'resources/resistance.png'
                            },
                            {
                                xtype: 'label',
                                html: 'Zs',
                                margin: '0 47 0 5',
                                style: {
                                    'font-style': 'italic',
                                    'font-size': '15px',
                                    color: '#000'
                                }
                            },
                            {
                                xtype: 'numberfield',
                                flex: 1,
                                margin: '0 15 0 0',
                                style: {
                                    'text-align': 'center'
                                },
                                width: '',
                                labelWidth: '0%',
                                name: 's',
                                placeHolder: 'Enter Zs...'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        items: [
                            {
                                xtype: 'label',
                                docked: 'right',
                                html: 'Ω',
                                margin: '0 20',
                                style: {
                                    color: '#999',
                                    'font-size': '12px',
                                    'font-style': 'italic'
                                }
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                margin: '15 10 5 10',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox'
                        },
                        items: [
                            {
                                xtype: 'button',
                                action: 'clear',
                                flex: 1,
                                margin: '0 0 0 10',
                                ui: 'action-round',
                                text: 'Clear'
                            },
                            {
                                xtype: 'container',
                                flex: 1
                            },
                            {
                                xtype: 'button',
                                action: 'calculate',
                                flex: 3,
                                ui: 'confirm-round',
                                text: 'Calculate!'
                            }
                        ]
                    },
                    {
                        xtype: 'label',
                        name: 'error',
                        hidden: true,
                        html: 'Please enter two values',
                        margin: '5 0 0 0',
                        style: 'text-align:center;color:#f00'
                    }
                ]
            }
        ]
    }

});
/*
 * File: app/view/OrderTab.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.OrderTab', {
    extend: 'Ext.Container',
    alias: 'widget.ordertab',

    config: {
        syncTrigger: 'show',
        cls: 'card',
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'button',
                action: 'newOrderButton',
                docked: 'top',
                margin: '10 10 0 10',
                padding: 5,
                ui: 'action',
                iconCls: 'add',
                text: 'New Order'
            },
            {
                xtype: 'list',
                flex: 1,
                ui: 'round',
                disableSelection: true,
                emptyText: 'No Orders',
                itemTpl: [
                    '<img src="resources/supplies.png" width="30" style="float:left;padding-right:5px"/>',
                    '<div style="float:left">{refNum}</div>',
                    '<img src="resources/css/cef.png" height="20" width="40" style="float:right"/>',
                    '<div style="clear:both;padding-left:30px">{dateCreated:date("D d-m-Y")}</div>'
                ],
                store: 'OrderStore'
            }
        ]
    }

});
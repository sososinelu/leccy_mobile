/*
 * File: app/view/ClientTab.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.ClientTab', {
    extend: 'Ext.Container',
    alias: 'widget.clienttab',

    config: {
        syncTrigger: 'show',
        cls: 'card',
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'button',
                action: 'newClientButton',
                docked: 'top',
                margin: '10 10 0 10',
                padding: 5,
                ui: 'action',
                iconCls: 'add',
                text: 'Add Client'
            },
            {
                xtype: 'list',
                height: '100%',
                ui: 'round',
                masked: false,
                disableSelection: true,
                emptyText: 'No Clients',
                itemTpl: Ext.create('Ext.XTemplate', 
                    '<div style="margin-left: -10px;">   ',
                    '<table> ',
                    '	<tr>',
                    '		<td rowspan="1">',
                    '        	<img width="40" height="40" src="resources/user.png" />',
                    '        </td>',
                    '        <td align="center">',
                    '        	&nbsp;',
                    '        </td>',
                    '        <td width>',
                    '        	&nbsp;',
                    '        </td>',
                    '        <td align="center">      ',
                    '        	<table> ',
                    '            	<tr>',
                    '                	<td>',
                    '                    	{[this.getClientName(values.forename, values.surname, values.company)]} ',
                    '                	</td>     	',
                    '                </tr>',
                    '',
                    '          	</table> ',
                    '        </td>			',
                    '	</tr>',
                    '</table> ',
                    '</div>',
                    {
                        getClientName: function(forename, surname, company) {
                            if (forename === "" && surname === ""){
                                return company;
                            } else {
                                return forename + " " + surname;
                            }
                        }
                    }
                ),
                loadingText: '',
                store: 'ClientStore'
            }
        ]
    }

});
/*
 * File: app/view/NavView.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.NavView', {
    extend: 'Ext.navigation.View',
    alias: 'widget.mainnav',

    requires: [
        'MyApp.view.Dashboard',
        'MyApp.view.AppLogo'
    ],

    config: {
        layout: {
            animation: false,
            type: 'card'
        },
        items: [
            {
                xtype: 'mytabpanel',
                title: 'Leccy'
            }
        ],
        navigationBar: {
            docked: 'top',
            id: 'navBarID',
            itemId: 'mynavigationbar',
            items: [
                {
                    xtype: 'button',
                    align: 'right',
                    disabled: false,
                    hidden: true,
                    id: 'navViewOptionButt',
                    padding: '3 12 3 12',
                    iconAlign: 'right',
                    text: 'Options'
                },
                {
                    xtype: 'button',
                    align: 'right',
                    disabled: false,
                    hidden: true,
                    id: 'navViewOptionButt1',
                    padding: '3 12 3 12',
                    iconAlign: 'right',
                    text: 'Save'
                },
                {
                    xtype: 'applogo',
                    hidden: false,
                    id: 'navViewAppLogo',
                    margin: '0 0 0 26'
                },
                {
                    xtype: 'image',
                    align: 'right',
                    height: 30,
                    hidden: true,
                    hideAnimation: 'fadeOut',
                    id: 'syncIconLogo',
                    margin: '0 5 0 0',
                    showAnimation: 'fadeIn',
                    width: 30,
                    src: 'resources/sync_arrows.png'
                }
            ]
        }
    }

});